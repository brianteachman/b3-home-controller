
###
# Alexa Controlled Raspberry Pi Home Controller
# 
# Author:  Brian Teachman
# Date:    12/8/2016
###

from __future__ import division # haha! This was a good one
from flask import Flask
from flask_ask import Ask, statement, question, convert_errors
import RPi.GPIO as GPIO
import logging
import time, datetime
import serial
import csv

## ------------------------------------------------------------------------- ##

MAIN_ROOM = 0
DINING_ROOM = 1

room_list = {'main': MAIN_ROOM, 'dining': DINING_ROOM}

# system codes
SYS_MESSAGE = 0
SYS_LIGHT = 1
SYS_HEAT = 2

# message codes (system=0)
MC_GET_TEMP = 0             # case 0
MC_IS_HEAT_ON = 1           # case 1
MC_GET_TEMP_SETPOINT = 2    # case 2
MC_IS_LIGHT_ON = 3          # case 3
                            # default case error 400

# light codes
MC_TURN_OFF_LIGHT = -1

# heat codes (system=2)
MC_TURN_OFF_HEAT = 0

ON = 1
OFF = 0

## ------------------------------------------------------------------------- ##

app = Flask(__name__)
ask = Ask(app, '/')

# log to console
logging.getLogger("flask_ask").setLevel(logging.DEBUG)

# log to file
logger = logging.getLogger("B3HC")
logger.setLevel(logging.DEBUG)
today = str( datetime.date.today() )
fh = logging.FileHandler("data/"+today+"-debug.log")
fh.setLevel(logging.DEBUG)
fm = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(fm)
logger.addHandler(fh)

## ------------------------------------------------------------------------- ##

ser = serial.Serial(
    port='/dev/ttyS0',  # GPIO UART on Raspberry Pi 3
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
    )

## ------------------------------------------------------------------------- ##

sample_ask = """ 
Some thing's you can say: 

Turn on the dining room lights.
Turn the main room lights off.
Set the main room light to 50 percent.
Set the lights to 100 percent in the dining room.

What's the temperature in the main room?
What is the temperature in the dining room?
What temperature is it in the dining room?
Tell me the temperature in the main room.
"""

@ask.launch
def welcome_message():
    
    logger.info('Home Controller initiated.')

    return question("Welcome to the Home Controller. What would you like to do?") \
           .reprompt("I'm sorry, what would you like to do?") \
           .standard_card(title='B3 Smart Home Controller', 
                          text=sample_ask,
                          large_image_url='https://d30y9cdsu7xlg0.cloudfront.net/png/132885-200.png')


@ask.intent('AMAZON.HelpIntent')
def help_message():
    help_message = """ 
    Thing's you can say: 

    Turn on the dining room lights.
    Turn the main room lights off.

    Or try,

    Set the main room light to 50 percent.
    Set the lights to 100 percent in the dining room.

    Now, what would you like to do?
    """
    logger.info("HelpIntent called.")
    return question(help_message).reprompt(help_message) \
           .simple_card(title='Home Controller Command List', content=sample_ask)


@ask.intent('SwitchRoomLightIntent', mapping={'room': 'room', 'status': 'status'})
def switch_room_light(room, status):
    ### Sample utterances:
    # Turn the {room} room light {status}
    # Turn {status} the {room} room light
    ###
    logger.info("SwitchRoomLightIntent called.")

    if room not in room_list:
        logger.debug('SwitchRoomLightIntent: Some room called %s was called.', room)
        return question("That was not a room I'm acquainted with, please make your request again")

    if status in ['on', 'high']:    state = 255
    if status in ['off', 'low']:    state = 0

    # if room !== None:   tx(device, 1, state) # Light system = 1
    tx(device_id(room), SYS_LIGHT, state) # Light system = 1

    logger.debug('SwitchRoomLightIntent: Turning light in %s room %s. analogWrite(ledPin, %s)', room, status, state)

    title = room.capitalize() + " Room Lighting"
    return statement( 'Turning light in {} room {}'.format(room, status) ) \
           .simple_card(title=title, content='The light in the {} room is {}.'.format(room, status))


@ask.intent('SetRoomLightPercentIntent', convert={'room': str, 'percent': int})
def set_room_light(room, percent):
    ### Sample utterances:
    # Set the {room} room light to {percent} percent
    # Set the lights to {percent} percent in the {room} room
    ###
    logger.info("SetRoomLightPercentIntent called.")

    if room not in room_list:
        logger.debug('SetRoomLightPercentIntent: Some room called %s.', room)
        return question("That's not a room I'm acquainted with, please make your request again")

    # scale percentage to room controller light level
    light_level = ( percent / 100 ) * 255

    # transmit packet to room controllers
    # tx(device_id(room), SYS_LIGHT, light_level)
    rx_response = fetch_response(device_id(room), SYS_LIGHT, light_level)

    logger.debug('SetRoomLightPercentIntent: Light level at %s percent set to: %s. (percent is: %s, light_level: %s)', percent, light_level, type(percent), type(light_level))

    title = room.capitalize() + " Room Lighting"
    return statement('Setting {} room light to {} percent'.format(room, percent)) \
           .simple_card(title=title, content='Light in the {} room is set to {}%. Or {} on a scale from 0-255.'.format(room, percent, light_level))


@ask.intent('IsLightInRoomOnIntent', mapping={'room': 'room', 'status': 'status'})
def room_heat_status(room):
    ### Sample utterances:
    # Is the light in the dining room on
    # Is the light in the dining room off
    # Is the light on in the main room 
    ###
    logger.info("IsLightInRoomOnIntent called.")

    if room not in room_list:
        logger.debug('IsLightInRoomOnIntent: Some room called %s was called.', room)
        return question("That's not a room I'm acquainted with, please make your request again")

    rx_response = fetch_response(device_id(room), SYS_MESSAGE, MC_IS_LIGHT_ON)  # fetch light state
    light_state = rx_response['state']

    if light_state == ON:
        return_statement = 'The light in the {} room is on'.format(room)
    else:
        return_statement = 'The light in the {} room is off'.format(room)

    logger.debug('IsLightInRoomOnIntent: The light in the %s room is %s  (%s)', room, light_state, bool(light_state))

    title = "Smart Home Lighting"
    return statement(return_statement) \
            .simple_card(title=title, content=return_statement)


@ask.intent('WhatIsTempSetpointIntent', mapping={'room': 'room'})
def get_room_temp(room):
    ### Sample utterances:
    # What is the temperature set to in the main room
    # What is the dining room temperature set to
    # What is the temperature set point for the main room
    ###
    logger.info("WhatIsTempSetpointIntent called.")

    if room not in room_list:
        logger.debug('WhatIsTempSetpointIntent: Some room called %s was called. reprompted user.', room)
        return question("That was not a room I'm acquainted with, please make your request again")

    rx_response = fetch_response(device_id(room), SYS_MESSAGE, MC_GET_TEMP_SETPOINT)  # fetch temperature from room controller
    temp = rx_response['state']

    if temp and (temp != 400):
        return_statement = 'The set point for the temperature in the {} room is set to {} degrees fahrenheit'.format(room, temp)
        logger.debug('GetRoomTemperatureIntent: '+return_statement)
    elif temp == 0:
        return_statement = 'The set point is not set for the temperature in the {} room'.format(room)
        logger.error('GetRoomTemperatureIntent: '+return_statement)
    else:
        return_statement = 'There was a problem getting the temperature from the {} room'.format(room)
        logger.error('GetRoomTemperatureIntent: '+return_statement)

    title = room.capitalize() + " Room Temerature Set Point"
    return statement(return_statement) \
            .simple_card(title=title, content=return_statement)


@ask.intent('GetRoomTemperatureIntent', mapping={'room': 'room'})
def get_room_temp(room):
    ### Sample utterances:
    # What is the temperature in the main room
    # Tell me the temperature in the dining room
    ###
    logger.info("GetRoomTemperatureIntent called.")

    if room not in room_list:
        logger.debug('GetRoomTemperatureIntent: Some room called %s was called. reprompted user.', room)
        return question("That was not a room I'm acquainted with, please make your request again")

    rx_response = fetch_response(device_id(room), SYS_MESSAGE, MC_GET_TEMP)  # fetch temperature from room controller
    temp = rx_response['state']

    if temp and ( temp not in [400, 500] ):
        return_statement = 'The temperature in the {} room is {} degrees fahrenheit'.format(room, temp)
        logger.debug('GetRoomTemperatureIntent: '+return_statement)
    else:
        return_statement = 'There was a problem getting the temperature from the {} room'.format(room)
        logger.error('GetRoomTemperatureIntent: '+return_statement)

    title = room.capitalize() + " Room Heating"
    return statement(return_statement) \
            .simple_card(title=title, content=return_statement)


@ask.intent('SetRoomTemperatureIntent', mapping={'room': 'room', 'temp': 'temp'})
def set_room_temp(room, temp):
    ### Sample utterances:
    # Set the temperature in the main to 75 degrees
    # Set the temperature to 75 in the dining room
    # Set the heat in the dining room to 68
    ###
    logger.info("SetRoomTemperatureIntent called.")

    if room not in room_list:
        logger.debug('SetRoomTemperatureIntent: Some room called %s was called. reprompted user.', room)
        return question("That's not a room I'm acquainted with, please make your request again")

    rx_response = fetch_response(device_id(room), SYS_HEAT, temp)  # set temperature from room controller
    is_set = rx_response['state']

    if is_set and ( is_set not in [400, 500] ):
        return_statement = 'The {} room temperature has been set to {} degrees fahrenheit'.format(room, temp)
    else:
        return_statement = 'There was a problem setting the {} room temperature.'.format(room)
    
    logger.error('SetRoomTemperatureIntent: '+return_statement)

    title = room.capitalize() + " Room Heating"
    return statement(return_statement) \
            .simple_card(title=title, content=return_statement)


@ask.intent('TurnOffAllHeatersIntent')
def turn_off_heaters():
    ### Sample utterances:
    # Turn the {room} room light {status}
    # Turn {status} the {room} room light
    ###
    logger.info("TurnOffAllHeatersIntent called.")

    tx(MAIN_ROOM, SYS_HEAT, MC_TURN_OFF_HEAT)
    tx(DINING_ROOM, SYS_HEAT, MC_TURN_OFF_HEAT)

    logger.debug('TurnOffAllHeatersIntent: All heaters off')

    title = "Smart Home Heating"
    return statement( 'Turning heaters in all rooms off.' ) \
           .simple_card(title=title, content='Turning heaters in all rooms off.')


@ask.intent('IsHeatOnInRoomIntent', mapping={'room': 'room'})
def room_heat_status(room):
    ### Sample utterances:
    # Is the heater in the dining room running
    # Is the heater in the dining room on
    # Is the heater running in the main room 
    ###
    logger.info("IsHeatOnInRoomIntent called.")

    if room not in room_list:
        logger.debug('IsHeatOnInRoomIntent: Some room called %s was called. reprompted user.', room)
        return question("That was not a room I'm acquainted with, please make your request again")

    rx_response = fetch_response(device_id(room), SYS_MESSAGE, MC_IS_HEAT_ON)  # fetch heater state
    heater_state = rx_response['state']

    if heater_state == ON:
        return_statement = 'The heater in the {} room is on'.format(room)
    else:
        return_statement = 'The heater in the {} room is off'.format(room)

    logger.debug('IsHeatOnInRoomIntent: The heater in the %s room is %s  (%s)', room, heater_state, bool(heater_state))

    title = "Smart Home Heating"
    return statement(return_statement) \
            .simple_card(title=title, content=return_statement)


@ask.intent('AreAnyHeatersOnIntent')  
def any_heat_status():
    ### Sample utterances:
    # Tell me if any heaters are running
    # Are any heaters running
    # Are there any heaters running
    # Are there heaters running in any rooms
    # Are there heaters running in any of the rooms
    ###
    logger.info("AreAnyHeatersOnIntent called.")

    main_room = fetch_response(MAIN_ROOM, SYS_MESSAGE, MC_IS_HEAT_ON)  # fetch heater state

    time.sleep(0.1) # give some time for propagation

    dining_room = fetch_response(DINING_ROOM, SYS_MESSAGE, MC_IS_HEAT_ON)  # fetch heater state

    logger.debug('AreAnyHeatersOnIntent: Main room: %s', main_room)
    logger.debug('AreAnyHeatersOnIntent: Dining room: %s', dining_room)

    if ( main_room['state'] == ON ) and ( dining_room['state'] == ON ):
        return_statement = 'The heater is currently running in both rooms'

    elif main_room['state'] == ON:
        return_statement = 'The heater in the main room is on'

    elif dining_room['state'] == ON:
        return_statement = 'The heater in the dining room is on'

    elif ( main_room['state'] == 500 ) or ( dining_room['state'] == 500 ):
        return_statement = 'There was a problem reading a temperature sensor'

    else:
        return_statement = 'There are currently no heaters running'

    return statement(return_statement) \
            .simple_card(title='Smart Home Heater Status', content=return_statement)


## ------------------------------------------------------------------------- ##

def device_id(room):
    ## Set device id, either 0 or 1 (1 of 2 devices)
    return MAIN_ROOM if room == 'main' else DINING_ROOM

def csv_format(device, system, state):
    ## Return csv formated string: "device,system,state"
    #  device: 0=Main, 1=Dining
    #  system: 0=Messaging, 1=Lighting, 2=Heating
    return str(device) + ',' + str(system) + ',' + str(state)

def csv_parse(serial_string):
    csv_strings = csv.reader([serial_string])
    return list( map( int, list(csv_strings)[0]))

def tx(device, system, state):
    ## Send message to Room Controller over serial port
    payload = csv_format(device, system, state)

    ser.flushInput()
    ser.flushOutput()

    ser.write( payload )
    logger.debug('Tx: Sending message: %s', payload)

rx_retry = 0 # Rx retry counter

def rx():
    ## Recieve and parse message from Room Controller, via serial
    csv_packet = ser.readline().rstrip()   # fetch serial and strip "\r\n"
    return csv_parse(csv_packet)       # parse csv


def fetch_response(device, system, state):
    ## Transmit and recieve serial message
    global rx_retry # use global counter

    tx(device, system, state)  # send request payload

    time.sleep(.5)             # give the serial port time to receive the data

    rx_failed = True
    while rx_failed:
        csv_list = rx()            # return response payload from serial buffer

        if (len(csv_list) == 3):
            rx_failed = False

        # if there aren't three elements in the response, and we haven't already tried three times
        if (len(csv_list) < 3) and (rx_retry < 3):
            logger.debug('Rx: Retry=%s - Failed fetch: %s', rx_retry, csv_list)
            rx_retry += 1
            time.sleep(.5)

        if (len(csv_list) < 3) and rx_retry == 3: # reset counter and return error status
            rx_retry = 0
            logger.error('Rx: TIMED OUT - %s', csv_list)
            return {'device': 1, 'system': 0, 'state': 400}

    rx_retry = 0
    # ser.flushInput()
    # ser.flushOutput()

    logger.debug('Rx: Incoming message: %s', csv_list)

    return {'device': csv_list[0], 'system': csv_list[1], 'state': csv_list[2]}


## ------------------------------------------------------------------------- ##

if __name__ == '__main__':
    port = 5000 # set same as tunnel
    app.run(host='0.0.0.0', port=port)
